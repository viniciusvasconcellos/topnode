<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Formulário</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
		<script src="dist/jquery.validate.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script>
			$().ready(function() {
				console.log('ready');
				$("#step_1").validate({
					rules: {
						nome : "required",
						cpf : "required",
						email : "required",
						telefone : "required"
					},
					messages :
					{
						nome: "O campo NOME deve ser preenchido",
						cpf : "O campo CPF deve ser preenchido",
						email : "O campo E-MAIL deve ser preenchido",
						telefone : "O campo TELEFONE deve ser preenchido"
					}
				})
				
			});
			var nome;
			var cpf;
			var email;
			var telefone;
			var regiao;
			var unidade;
			function completeForm(select)
			{	
				console.log('completeForm');
			}
			
			function teste()
			{	
				if($('#step_1').valid())
				{
					nome = $('#nome').val();
					cpf = $('#cpf').val();
					email = $('#email').val();
					telefone = $('#telefone').val();
					
					
					$('.form-step').hide().next().show();
					$('#step_sucesso').hide();
					$("#step_1").submit();
					return false;
					
				} else 
				{
					console.log("invalid")
				
				}
				
			}
		</script>
		
    </head>
    <body>
        <div class="container">
            <div class="row" style="margin:30px 0">
                
                <div class="col-lg-9">
                    <h3>Formulário de contato</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6" id="form-container">
					<?PHP
					
						if ($_SERVER['REQUEST_METHOD'] == "POST")
						{	
						
							require 'PHPMailer/PHPMailerAutoload.php';

							$mensagem = 'nome: '.$_POST['nome'].' cpf: '.$_POST['cpf'].' email: '.$_POST['email'].' telefone: '.$_POST['telefone'];
							$mail = new PHPMailer;
							$mail->isSendmail();
							$mail->setFrom('site@example.com', 'Formulario Site');
							$mail->addReplyTo('contato@example.com', 'Contato Site');
							$mail->addAddress('atendimento@example.com', 'Atendimento Site');
							$mail->Subject = 'Envio de formulário';
							$mail->Body = $mensagem;
							
							//send the message, check for errors
							if (!$mail->send()) {
								//echo "Mailer Error: " . $mail->ErrorInfo;
							} else {
								echo "Message sent!";
							}

							?>
							   <div class="panel panel-info">
									<div class="panel-heading">
										<div class="panel-title">
										   Formulário enviado por email!
										</div>
									</div>
								</div>
							<?PHP
							
							exit();
						} else {
					?>
                    <form id="step_1" class="form-step" method="POST">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    Preencha seus dados para receber contato
                                </div>
                            </div>
                            <div class="panel-body">
                                <fieldset>
                                    <div class="row form-group">
                                        <div class="col-lg-6">
                                            <label>Nome Completo</label>
                                            <input class="form-control" type="text" name="nome" id="nome">
                                        </div>

                                        <div class="col-lg-6">
                                            <label>CPF</label>
                                            <input class="form-control" type="text" name="cpf" id="cpf">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-lg-6">
                                            <label>Email</label>
                                            <input class="form-control" type="text" name="email" id="email">
                                        </div>

                                        <div class="col-lg-6">
                                            <label>Telefone</label>
                                            <input class="form-control" type="text" name="telefone" id="telefone">
                                        </div>
                                    </div>

                                    <div>
                                        <input type="button" onclick='teste()' class="btn btn-lg btn-info next-step" value="Enviar">
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </form>
					<?PHP
					
						}
					?>
                </div>
               
            </div>
        </div>
      
    </body>
</html>
